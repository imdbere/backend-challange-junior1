using System.Linq;
using System.Threading.Tasks;
using BackendChallange.Database;
using BackendChallange.Database.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace BackendChallange.Controllers
{
    [Route("api/users/{userId}/portfolioEntries")]
    [ApiController]
    public class PortfolioEntriesController : ControllerBase
    {
        private readonly ILogger<PortfolioValuesController> _logger;
        private readonly ApplicationDBContext _context;

        public PortfolioEntriesController(ILogger<PortfolioValuesController> logger, ApplicationDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<PortfolioEntry>> GetPortfolioEntries(int userId)
        {
            var portfolioEntries = await _context.PortfolioEntries
                .Where(pv => pv.User.Id == userId)
                .ToListAsync();

            return portfolioEntries;
        }
    }
}