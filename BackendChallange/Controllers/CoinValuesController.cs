using System.Linq;
using System.Threading.Tasks;
using BackendChallange.Database;
using BackendChallange.Database.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace BackendChallange.Controllers
{
    [Route("api/users/{userId}/coinValues")]
    [ApiController]
    public class CoinValuesController : ControllerBase
    {
        private readonly ILogger<CoinValuesController> _logger;
        private readonly ApplicationDBContext _context;

        public CoinValuesController(ILogger<CoinValuesController> logger, ApplicationDBContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet("{coin}")]
        public async Task<IEnumerable<CoinValue>> GetCoinValues(int userId, string coin, DateTime? from, DateTime? to)
        {
            
        }
    }
}