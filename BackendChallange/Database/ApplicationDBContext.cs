﻿using BackendChallange.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackendChallange.Database
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<PortfolioEntry> PortfolioEntries { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("citext");

            // Needed for timescaledb
            modelBuilder.HasPostgresExtension("timescaledb");

            // Also needed for timescaledb, PK has to include time column
            modelBuilder.Entity<CoinValue>()
                .HasKey(e => new { e.Id, e.Time});

            modelBuilder.Entity<PortfolioValue>()
                .HasKey(e => new { e.Id, e.Time});



            // Test data (modify it if you want)
            modelBuilder.Entity<User>()
                .HasData(new User 
                {
                    Id = 1,
                    UserName = "wilson"
                });

            modelBuilder.Entity<PortfolioEntry>()
                .HasData(new PortfolioEntry 
                {
                    Id = 1,
                    UserId = 1,
                    Coin = "BTC",
                    Quantitiy = 0.005M
                });

            modelBuilder.Entity<PortfolioEntry>()
                .HasData(new PortfolioEntry 
                {
                    Id = 2,
                    UserId = 1,
                    Coin = "ETH",
                    Quantitiy = 0.05M
                });
                
            modelBuilder.Entity<PortfolioEntry>()
                .HasData(new PortfolioEntry 
                {
                    Id = 3,
                    UserId = 1,
                    Coin = "ADA",
                    Quantitiy = 30M
                });
        }
    }
}
