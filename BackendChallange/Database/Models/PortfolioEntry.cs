using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendChallange.Database.Models
{
    public class PortfolioEntry
    {
        public int Id { get; set; }

        public DateTime LastChanged { get; set; }

        [Required]
        public User User { get; set; }
        public int UserId { get; set; }

        [Required]
        public string Coin { get; set; }

        [Required]
        public decimal Quantitiy { get; set; }
    }
}